@extends('layouts.app')

@section('content')

    @foreach($discussions as $discussion)
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <img src="{{$discussion->user->avatar}}" alt="user avatar" width="40px" height="40px">&nbsp;&nbsp;
                <span>{{$discussion->user->name}}, <b>{{$discussion->created_at->diffForHumans()}}</b></span>
                <a href="{{route('discussion',['slug' => $discussion->slug])}}"
                   class="btn btn-default pull-right btn-xs">view</a>
                @if($discussion->hasBestAnswer())
                    <span class="btn btn pull-right btn-danger btn-xs" style="margin-right: 8px">closed</span>
                @else
                    <span class="btn btn pull-right btn-success btn-xs" style="margin-right: 8px">open</span>
                @endif
            </div>
            <div class="panel panel-body">
                <h4 class="text-center">
                    {{$discussion->title}}
                </h4>
                <p class="text-center">
                    {{str_limit($discussion->content,50)}}
                </p>
            </div>
            <div class="panel-footer">
                 <span>
                     {{$discussion->replies->count()}} Replies
                 </span>
                <a href="{{route('channel',['slug' => $discussion->channel->slug])}}"
                   class="pull-right btn btn-default btn-xs">{{$discussion->channel->title}}</a>
            </div>
        </div>
    @endforeach
    <div class="text-center">
        {{$discussions->links()}}
    </div>

@endsection
