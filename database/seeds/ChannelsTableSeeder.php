<?php

use Illuminate\Database\Seeder;
use App\Channel;

class ChannelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $channel1 = ['title'=>'Laravel','slug' => str_slug('laravel')];
        $channel2 = ['title'=>'Vue.js','slug' => str_slug('vue.js')];
        $channel3 = ['title'=>'HTML 5','slug' => str_slug('html_5')];
        $channel4 = ['title'=>'CSS3','slug' => str_slug('css3')];
        $channel5 = ['title'=>'Angular','slug' => str_slug('angular')];
        $channel6 = ['title'=>'React','slug' => str_slug('react')];

        Channel::create($channel1);
        Channel::create($channel2);
        Channel::create($channel3);
        Channel::create($channel4);
        Channel::create($channel5);
        Channel::create($channel6);
    }
}
