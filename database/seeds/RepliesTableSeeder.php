<?php

use Illuminate\Database\Seeder;
use App\Reply;

class RepliesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $r1 =[
            'user_id' => 1,
            'discussion_id' => 1,
            'content' => 'testing reply',
        ];
        $r2 =[
            'user_id' => 2,
            'discussion_id' => 2,
            'content' => 'testing reply',
        ];
        $r3 =[
            'user_id' => 1,
            'discussion_id' => 2,
            'content' => 'testing reply',
        ];
        $r4 =[
            'user_id' => 2,
            'discussion_id' => 1,
            'content' => 'testing reply',
        ];
        $r5 =[
            'user_id' => 1,
            'discussion_id' => 1,
            'content' => 'testing reply',
        ];
        Reply::create($r1);
        Reply::create($r2);
        Reply::create($r3);
        Reply::create($r4);
        Reply::create($r5);
    }
}
