<?php

use Illuminate\Database\Seeder;
use App\Discussion;

class DiscussionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $t1 = 'testing discuss1';
        $t2 = 'testing discuss2';
        $t3 = 'testing discuss3';
        $t4 = 'testing discuss4';
        $t5 = 'testing discuss5';

        $d1 = [
            'title' => $t1,
            'content' => 'testing content',
            'channel_id' => 1,
            'user_id' => 2,
            'slug' => str_slug($t1),
        ];
        $d2 = [
            'title' => $t2,
            'content' => 'testing content',
            'channel_id' => 2,
            'user_id' => 1,
            'slug' => str_slug($t2),
        ];
        $d3 = [
            'title' => $t3,
            'content' => 'testing content',
            'channel_id' => 2,
            'user_id' => 1,
            'slug' => str_slug($t3),
        ];
        $d4 = [
            'title' => $t4,
            'content' => 'testing content',
            'channel_id' => 3,
            'user_id' => 2,
            'slug' => str_slug($t4),
        ];
        $d5 = [
            'title' => $t5,
            'content' => 'testing content',
            'channel_id' => 5,
            'user_id' => 1,
            'slug' => str_slug($t5),
        ];
        Discussion::create($d1);
        Discussion::create($d2);
        Discussion::create($d3);
        Discussion::create($d4);
        Discussion::create($d5);
    }
}
