<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name' => 'Admin',
            'password' => Hash::make('admin'),
            'email' => 'admin@forum.com',
            'admin' => 1,
            'avatar' => asset('/avatars/avatar.png'),
        ]);
        App\User::create([
            'name' =>'testing user',
            'password' => Hash::make('user'),
            'email' => 'user@forum.com',
            'avatar' => asset('/avatars/avatar.png'),
        ]);
    }
}
